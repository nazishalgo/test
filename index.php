<?php
require_once __DIR__ . '/src/HttpService.php';

use nazishfraz\HttpService;

$response = HttpService::processRequest([
    "method" => 'POST',
    'url' => 'https://nazish-php-dev.evalexpert.io/v1/cron/updateUsersEvaluation'
]);
echo json_encode($response);