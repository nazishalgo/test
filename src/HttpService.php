<?php
namespace nazishfraz;

use Exception;

interface HttpServiceInterface {
    public static function processRequest(array $settings): array;
}
class HttpService implements HttpServiceInterface
{
    /**
     * function for http curl request
     * @param array $settings [url, headers, method, body, timeout]
     * @return array response
     */
    public static function processRequest(array $settings): array
    {
        // validation
        if(!isset($settings["url"])) throw new Exception("url is required.");

        $url = $settings['url'];
        $headers = isset($settings['headers']) ? $settings['headers'] : [];
        $method = isset($settings["method"]) ? strtoupper($settings['method']) : "GET" ;
        $timeout = isset($settings["timeout"]) ? $settings["timeout"] : 60;
        $body = isset($settings["body"]) ? $settings['body'] : [];

        $allowedMethods =["GET","POST","PUT","DELETE","PATCH"];
        if (!in_array($method, $allowedMethods)) {
            throw new Exception("Invalid Method, allowed methods are: ".json_encode($allowedMethods));
        }

        $mergedHeaders = strtolower(implode("\n", $headers));

        if(strpos($mergedHeaders,"application/json") !== false) {
            $body = json_encode($body);
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => $timeout,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_HEADER => true,
            ));

        $response = curl_exec($curl);
 
        if (curl_errno($curl)) {
            curl_close($curl);
            throw new Error("HTTP REQUEST FAILED".curl_error($curl) );
        }

        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        curl_close($curl);

        $header = trim(substr($response, 0, $header_size));
        $body = trim(substr($response, $header_size));
      
        $jsonBody = json_decode($body,true);

        if(json_last_error() === JSON_ERROR_NONE) {
            $body =  $jsonBody;
        }
        $headersSplitted = explode("\r\n",$header);
        $headers = [];
        foreach($headersSplitted as $header) {
            $splitted = explode(":",$header);
            $key = trim($splitted[0]);
            array_shift($splitted);
            $value = trim(implode(":",$splitted));
            $headers[$key] = $value;
          
        }
        $response =  [
            "headers" =>  $headers,
            "body" =>  $body,
        ];

        return $response;
    }
}